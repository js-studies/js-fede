//Uma variavel declarada dentro de um if, vai rodar do lado de fora?

if (true) {
    var serie2 = "simpsons";
}
//como a serie2 foi declarado como var, é global, vai rodar
console.log(serie2);


//---------------agora com let

//global
var serie = "lost"

if (true) {
    //local
    let serie = "house";
    console.log(serie)
}
console.log(serie)
//valores distintos, pois os escopos estao separados